const { merge } = require('webpack-merge');
const common = require('./webpack.common.js');
const path = require('path');

module.exports = merge(common, {
  mode: 'development',
  devServer: {
    static: {
      directory: path.join(__dirname, 'public'),
    },
    compress: true,
    port: 9004,
  },  
  output: {
    path: path.join(__dirname, 'public'),
    filename: 'ap-datahandler.js',
    library: 'ApDatahandler',
    libraryTarget: 'umd',
    libraryExport: 'default',
  },  

});