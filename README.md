
## Parsing Limesurvey Data

You can use this library to parse surveys from Limesurvey and preppare the data for chart.js
There are currently two ways to use this library.

1. directly include ap-datahandler.js
2. install as a package via npm git

---

## ap-datahandler.js

Include the file via a html script tag. 

    <script src="ap-datahandler.js"></script>

After the script is loaded the object 'apDatahandler' will be available

---

## Using npm

This package can be installed via NPM using a git as repo. The library is for 

    npm install bitbucket:apportal/chartdatahandler

After the install the apDataHandler can be imported as DataHandler

    import { DataHandler } from 'ap-datahandler'

---

# Using the Library

To Prepare/Parse Data the data needs to be loaded on beforehand. Some surveys require two different models to be loaded. 

The library has a standard set of configurations for the surveys. The config file is located at /src/config.json

it contains objects for the different surveys - ordered by their id. Each object contains:

    "298749": {
      "type": "alsfrs",
      "debug": "demo/test-data-result.json",
      "help": "help/alsfrs_r.html",
      "title": "ALSFRS-r",
      "subtitle": "(Amyotrophic Lateral Sclerosis Functional Rating Scale - revised)",
      "legend_up": "viele motorische Funktionen",
      "legend_down": "wenige motorische Funktionen",
      "data_maxvalue": 48,
      "data_datefieldindex": 1,
      "datarange_start": 7,
      "datarange_end": 20,
      "datarange_indexs": [7,8,9,10,12,13,14,15,16,17,18,19,20],
      "datalabels": [
        "1. Sprache",
        "2. Speichelfluss",
        "3. Schlucken",
        "4. Handschrift",
        "5a. Essen schneiden und Besteck handhaben",
        "5b. Ernährungssonde und Utensilien handhaben",
        "6. Ankleiden und Körperpflege",
        "7. Umdrehen im Bett und Richten der Bettdecke",
        "8. Gehen",
        "9. Treppensteigen",
        "10. Luftnot",
        "11. Luftnot im Liegen",
        "12. Atemhilfen"
      ]
    },


The config is already included when the library is loaded - but can be overwritten by using

    apDataHandler.setConfig( {"298749":{
      "title": "This is the new title"
    }})

An example of loading and parsing data could look like this.

      //parse alsfrs data using callback
      async function get_alsfrs(){
        const response = await fetch('demo/test-data-2.json')
        const data     = await response.json()
        const parseOptions = {
          surveyid: data.surveyId,
          raw: data 
        }
        apDatahandler.parse(parseOptions,(data)=>{
          console.log('ALSFRS:',data)
        })
      }

For more examples please see /public/index.html

