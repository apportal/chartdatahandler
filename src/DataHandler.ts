import Def from './utilities';
import { ChartDataChannel, SurveyOptions, DataSetLabels, DataSets } from './datatypes';
import { ParseOptions } from './datatypes/ParseOptions';

/**
 *
 *
 * @export
 * @class dataHandler
 */
export default class dataHandler {

  protected DATA: DataSets;
  public RAW_DATA;
  public surveyOptions: SurveyOptions;

  constructor(parSOptions) {
    this.surveyOptions = parSOptions;
  }


  /**
   * 
   * @param parOpt 
   */
  public getDataMeta(parOpt) {
    return this.DATA[parOpt];
    //this.DATA.description         
  }


  /**
   * 
   * @param parRawData 
   */
  public parse(parRawData:any, parSOptions:ParseOptions, parCallback?:Function){
    parCallback.call(this, null)
  }


  /**
   * 
   */
  public getDataTitles(): DataSetLabels[] {
    return [];
  }



  /**
   * 
   * @param parIds 
   * @param parStart 
   * @param parEnd 
   */
  public getDataSets() {
    return this.DATA.dataItems;
  }





  /**
   * 
   * @param parStart 
   * @param parEnd 
   */
  public getDataLabels(parStart: number = 0, parEnd: number = 9999): string[] {
    return [];
  }


  /**
   * 
   * @param parItem 
   * @param parColor 
   * @param parLineAlpha 
   * @param parFillAlpha 
   */
  protected applyColor(parItem, parColor: any, parLineAlpha, parFillAlpha) {
    var col = (typeof parColor == 'number') ? Def.chartColorList[parColor] : parColor;
    parItem.backgroundColor = Def.convertHex(col, parFillAlpha);
    parItem.borderColor = Def.convertHex(col, parLineAlpha);
  }


  /**
   * 
   * @param parItem 
   * @param parProps 
   */
  protected applyProp(parItem, parProps: any) {
    for (var a in parProps) {
      parItem[a] = parProps[a];
    }
  }


  /**
   * This takes the RAW dataresonse as string. 
   * - First it decodes the string from Unicode
   * - Removes any Formulars from the legend row
   * - Replaces all linebreaks with brackes so the string can be parsed as a Json object
   * - All illigal rows (legend) are removed from dataset
   * - at the end, that data is sorted by the Date index number from the config.
   * @param parDataStr 
   */
  protected dataToObject(parDataStr: string): String[][] {

    var dataDecodede = this.b64DecodeUnicode(parDataStr);
    var dataObj: string[][];

    // autodetect if , or ; are used as seperators
    if (dataDecodede.indexOf('","') === -1 && dataDecodede.indexOf('";"') !== -1){
      console.log('SEPERATOR = ;');
      dataDecodede = dataDecodede.replace(new RegExp(/\";\"/gm), '","');
    }else{
      console.log('SEPERATOR = ,');
    }

    //replace fomulars in titles
    dataDecodede = dataDecodede.replace(new RegExp(/\"\{(.|n)*?\}\"/gm), '"Formular"');
    dataDecodede = dataDecodede.replace(new RegExp(/(\[|\{)/, 'g'), '(');
    dataDecodede = dataDecodede.replace(new RegExp(/(\]|\})/, 'g'), ')');

    //replace breaks with array brackets
    var dataArr = dataDecodede.trim().split('\n');
    dataObj = []
    //console.log(dataDecodede)

    dataArr.forEach( (li)=>{ 
      try {
        dataObj.push( JSON.parse(`[${li}]`) ) 
      } catch (e) {
        console.error('DATA ERROR', e,li);
      }
  
    })

    
    // dataDecodede = "[[" + dataDecodede.replace(new RegExp("\n", 'g'), '],[') + "]]";

    // //try to convert the data to a object with json decode
    // try {
    //   dataObj = JSON.parse(dataDecodede);
    // } catch (e) {
    //   console.error('DATA ERROR', dataDecodede);
    //   return [];
    // }

    if (Def.DEBUGMODE || true) {
      this.logColumnTitles(dataObj[0]);
    }

    //filter out bad records
    dataObj = dataObj.filter((a) => {
      if (a.length == 0) {
        console.error('DATASET-ERROR', 'data-length is 0');
        return false;
      }
      if (a[1] == '') {
        console.error('DATASET-ERROR', 'Data has not been completed (ABGESCHLOSSEN)');
        console.log(a)
        return false;
      }
      if (a[0] == 'id') {
        console.warn('DATASET-SKIPPING', 'found legend');
        return false;
      }
      return true;
    });

    var dIndex = this.surveyOptions.data_datefieldindex;

    //sort the data by date
    dataObj = dataObj.sort((a, b) => {
      //var aa = new Date(a[1]);
      //var bb = new Date(b[1]);
      //var aa = this.buildLabel(a[1]);
      //var bb = this.buildLabel(b[1]);
      var aa = this.buildLabel(a[dIndex]);
      var bb = this.buildLabel(b[dIndex]);
      return aa < bb ? -1 : aa > bb ? 1 : 0;
    });

    return dataObj;
  }



  /**
   * 
   * @param parColumns 
   */
  logColumnTitles(parColumns: string[]) {
    console.log(parColumns);
  }



  /**
   * 
   * 
   * @returns 
   * @memberof dataLoader
   */
  buildDataStructure(dataArryLength): ChartDataChannel[] {

    // get copy of labels from config. 
    // Labels = The types of data - NOT the labels of X or Y
    var parLabels = this.surveyOptions.datalabels.slice();

    var res: ChartDataChannel[] = [];
    var a;
    for (a = 0; a < parLabels.length; a++) {
      res.push({
        "label": parLabels[a],
        "data": new Array(dataArryLength),
        "backgroundColor": "#000"
      });
    }
    return res;
  }


  /**
   * Itterate over data to identify the horizontal Labels
   * @param ls_dataObj 
   */
  getHorizontalLabels(ls_dataObj) {

    //build labels
    var dataLabelsObj = {};
    var dataLables = [];
    var dIndex = this.surveyOptions.data_datefieldindex;

    // we first build a object with labels and count each item
    for (var a = 0; a < ls_dataObj.length; a++) {
      if (ls_dataObj[a][0] == 'id') {
        continue;
      }
      //var labelId = this.buildLabel( ls_dataObj[a][1] );
      var labelId = this.buildLabel(ls_dataObj[a][dIndex]);
      dataLabelsObj[labelId] = dataLabelsObj[labelId] ? dataLabelsObj[labelId] + 1 : 1;
    }

    for (var b in dataLabelsObj) {
      dataLables.push(b);
    }

    return dataLables;

  }

  
  dateToLabelIndex(parRow, parLabels) {
    var dIndex = this.surveyOptions.data_datefieldindex;
    var fullDateTime = parRow[dIndex];
    var dataTime = this.buildLabel(fullDateTime);
    var dataIdx = parLabels.indexOf(dataTime);
    return dataIdx;
  }




  /**
   * 
   * 
   * @param {string} parDateString 
   * @returns 
   * @memberof dataLoader
   */
  public buildLabel(parDateString: String) {
    var sp = String(parDateString.split(' ')[0]).split('-');
    return sp[0] + '-' + sp[1];
  }


  /**
   * 
   * 
   * @param {any} str 
   * @returns 
   * @memberof dataLoader
   */
  protected b64DecodeUnicode(str) {
    // Going backwards: from bytestream, to percent-encoding, to original string.
    return decodeURIComponent(atob(str).split('').map(function (c) {
      return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));
  }



  /**
   * 
   * @param start Generate random date
   * @param end 
   */
  protected randomDate(start, end) {
    var dd = new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
    var f = function (v) {
      return (v < 10 ? '0' + v : v)
    }
    return dd.getFullYear() + '-' + f(dd.getMonth() + 1) + '-' + f(dd.getDate()) + ' ' + dd.getHours() + ':' + dd.getMinutes() + ':00';
  }


}