import * as defaultConfig from './config.json';
import dataHandler from './DataHandler';
import { SurveyOptions, DataSets } from './datatypes';
import { ParseOptions } from './datatypes/ParseOptions';
import { ALSFRS_Handler } from './handlers/ALSFRS_DataHandler';
import { MYMOP_Handler } from './handlers/MYMOP_DataHandler';
import { NFL_Handler } from './handlers/NFL_DataHandler';
import { TSQM_Handler } from './handlers/TSQM_DataHandler';
import utilities from './utilities';

type dataType = 'ALSFRS' | 'MYMOP' | 'NFL' | 'TSQM'; 

export * from './datatypes'

const parseConfig = (data:any) => {
  if (!data.copy) return data

  Object.keys(data.copy).forEach((key)=>{
    let entry = data[key]
    data.copy[key].forEach( (altkey)=>{
      data[altkey] = {...entry}
    })
  });
  return {...data}
}


export const DataHandler = {

  config: parseConfig(defaultConfig),

  setConfig(newConfig:any){
    var parsedConfig = parseConfig(newConfig);
    //console.log('MERGING NEW CONFIG',parsedConfig);
    console.log('v1.0');
    DataHandler.config = {...DataHandler.config,...parsedConfig}
  },

  getConfig():any{
    return DataHandler.config
  },

  getConfigSurveyIds():String{
    return Object.keys(DataHandler.config).join(',');
  },

  parse( data: ParseOptions,cb: Function ):Promise<DataSets>{

    if (!utilities.test(data,'Data Missing, at least [surveyid] must be given')) return
    if (!utilities.test(data.surveyid,`surveyid missing\nPossible Values: ${ DataHandler.getConfigSurveyIds()}`)) return
    if (!utilities.test( DataHandler.config[data.surveyid] ,`surveyid [${data.surveyid}] not found\npossible values: ${ DataHandler.getConfigSurveyIds() }.` )) return
    
    let curConfig:SurveyOptions = DataHandler.config[data.surveyid];
    let parser: dataHandler

    console.log('USING CONFIG', DataHandler.config);

    switch (curConfig.type.toUpperCase()){
      case 'ALSFRS':        
        parser = new ALSFRS_Handler( DataHandler.config[ data.surveyid ] )
        break
      case 'MYMOP':
        parser = new MYMOP_Handler( DataHandler.config[ data.surveyid ] )
        break
      case 'NFL':
        parser = new NFL_Handler( DataHandler.config[ data.surveyid ] )
        break
      case 'TSQM':
        parser = new TSQM_Handler( DataHandler.config[ data.surveyid ] )
        break
      default:
        console.warn('Type not defined :',curConfig.type )
    }

    if (cb){
      parser.parse( data.raw, data, (result)=>{
        cb(result)
      })
    }else{
      return new Promise( (resolve,reject)=>{
        parser.parse(data.raw, data,(result)=>{
          resolve(result)
        })
      })      
    }

  }


}