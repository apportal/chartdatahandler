/**
 * 
 * 
 * @export
 * @interface dataSetLabels
 */
 export interface DataSetLabels {
  id: number,
  selected: boolean,
  color: string,
  title: string
}

