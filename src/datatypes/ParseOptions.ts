export interface ParseOptions {
  surveyid: string
  raw: any
  followupData?: any
}