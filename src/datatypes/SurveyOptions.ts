
export interface SurveyOptions {
  type:string;
  debug:string;
  title:string;
  subtitle:string;
  
  legend_up:string;
  legend_down:string;

  followupData:string;
  data_maxvalue:number;
  data_datefieldindex:number;
  datarange_start?:number;
  datarange_end?:number;
  datarange_indexs?:number[];
  data_mapping?:any
  datalabels:string[];
  help:string;
}