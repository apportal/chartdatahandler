import { ChartDataChannelValue } from "./ChartDataChannelValue";

/**
 * The Chart Data Channel hold data and info for a channel with the data
 * 
 * @export
 * @interface ChartDataChannel
 */
 export interface ChartDataChannel {
  label: string;    
  data: ChartDataChannelValue[];

  backgroundColor?: string;
  borderColor?: string;

  // this is used by Vue
  id?: number;    
  sid?: string;
  selected?: boolean;
  _data?: ChartDataChannelValue [];
}