
/**
 *
 *
 * @export
 * @interface ChartDataChannelValue
 */


export interface ChartDataChannelValue {
    x: any;
    y: any;
}


