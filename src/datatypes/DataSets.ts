
import { SurveyOptions } from ".";
import { ChartDataChannel } from "./ChartDataChannel";

/**
 *
 *
 * @export
 * @interface dataSets
 */

 export interface DataSets {
  labels: string[];
  dataItems:  ChartDataChannel[];
  title: string;
  description: string;
  maxValue: number;
  recordCount:number;
  surveyOptions?: SurveyOptions  
}
