
export interface ErrorResponse {
  type: string;
  message: string;
}

export default class {

  /**
   * Test a value and return a default if value is not set
   * 
   * @param {any} parVal 
   * @param {any} parDefault 
   * @returns 
   * @memberof dataLoader
   */
  public static defaultVal(parVal, parDefault, parValName = '') {
    if (parVal) {
      return parVal
    } else {
      console.warn('(' + parValName + ') Value not set - default to:', parDefault);
      return parDefault
    }
  }

  public static test(value, errorText){
    if (typeof value === 'undefined'){
      console.log('ERROR: %c'+errorText,'color: #ffcc00' );
      return false
    }
    return true
  }


  public static blue: string = '#126398';

  public static chartColorList: string[] = [
    '#edc240',
    '#afd8f8',
    '#cb4b4b',
    '#4da74d',
    '#9440ed',
    '#bd9b33',
    '#8cacc6',
    '#8e2f2f',
    '#3d853d',
    '#cd4d4c',
    '#aae0ce',
    '#b15299',
    '#829350',
    '#4c6ecd',
    '#000000',
    '#000000',
    '#000000',
    '#000000',
    '#000000',
  ]

  /**
   *
   *
   * @static
   */
  public static defaultStyle = {
    fill: true,
    pointRadius: 3,
    pointHitRadius: 5,
  }


  /**
   * 
   * @param hex 
   * @param opacity 
   * @returns 
   */
  public static convertHex(hex, opacity) {
    hex = hex.replace('#', '');
    var r = parseInt(hex.substring(0, 2), 16);
    var g = parseInt(hex.substring(2, 4), 16);
    var b = parseInt(hex.substring(4, 6), 16);
    var result = 'rgba(' + r + ',' + g + ',' + b + ',' + opacity / 100 + ')';
    return result;
  }


  /**
   * 
   * @param parDate 
   * @returns 
   */
  public static mkDate(parDate: string) {
    var m = ['', 'Jan', 'Feb', 'Mär', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sept', 'Okt', 'Nov', 'Dez'];
    if (parDate.indexOf('-') != -1) {
      var d = parDate.split('-');
      return m[d[0]] + " " + d[1];
    }
    return parDate;
  }


  // public static surveyOptions:  config.SurveyOptions;

  public static DEBUGMODE: Boolean = false;

};
