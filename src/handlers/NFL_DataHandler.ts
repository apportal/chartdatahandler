
import Def from '../utilities';
import DataHandler from '../DataHandler';
import { ChartDataChannel, ChartDataChannelValue, DataSetLabels, DataSets } from './../datatypes'
import { ParseOptions } from '../datatypes/ParseOptions';
import * as dayjs from 'dayjs';

export class NFL_Handler extends DataHandler {

  private dataRange: number[];

  constructor(sOption) {
    super(sOption);
  }

  /**
   * 
   * @param parRawData 
   */
  public parse(parRawData, parOptions:ParseOptions, parCallback?:Function){
    this.DATA = this.parseLimesurveyRawData(parRawData);
    this.DATA.surveyOptions = this.surveyOptions;
    parCallback.call(this, this.DATA);
  }


  /**
   * Parse and prepare the chart datastructure
   * @param parRawData 
   */
  public parseLimesurveyRawData(parRawData: any): DataSets {
    var ls_dataObj = this.dataToObject(parRawData.responses);

    // build the horizontal labels list
    var dataLables: string[] = []
    var mapping = {}
    Object.keys(this.surveyOptions.data_mapping).forEach( k=>mapping[ this.surveyOptions.data_mapping[k] ] = k )

    var index = mapping['Datum'];

    for (var a = 0; a < ls_dataObj.length; a++) {
      dataLables.push(this.buildLabel(ls_dataObj[a][index]));
    }
    //console.log(mapping)
    // build the inital Datastructure
    var dataItems: ChartDataChannel[] = [];//this.buildDataStructure( ['dataLables'] );
    var dateOfBirth = '';

    dataItems.push({
      label: 'Data',
      data: ls_dataObj.map((obj) => {
        //{x:0,y:10},
        dateOfBirth = String(obj[ mapping['Geburtsdatum'] ]);
        return {
          x: dayjs(String(obj[ mapping['Datum'] ])).format('YYYY-MM'),
          y: obj[ mapping['Wert'] ],
          fulldate: dayjs(String(obj[ mapping['Datum'] ])),
          label: obj[ mapping['Einschäzung']],
        }
      })
    });


    // NOTE - we dont need this - only one recorde set 
    // //process each row and insert data into chart data object
    // for( var a=0; a < ls_dataObj.length; a++){
    //     this.insertData(ls_dataObj[a], dataItems, dataLables);
    // }

    return ({
      title: Def.defaultVal(this.surveyOptions.title, parRawData.title, 'surveyOptions.title'),
      description: Def.defaultVal(this.surveyOptions.subtitle, parRawData.welcomeText, 'surveyOptions.title'),
      dateOfBirth: dateOfBirth,
      maxValue: this.surveyOptions.data_maxvalue,
      labels: dataLables,
      recordCount: ls_dataObj.length,
      dataItems: dataItems
    } as DataSets);

  }


  /**
   * 
   * 
   * @param {string} parDateString 
   * @returns 
   * @memberof dataLoader
   */
  public buildLabel(parDateString: String) {
    var sp = String(parDateString.split(' ')[0]).split('-');
    return sp[0] + '-' + sp[1] + '-' + sp[2];
  }



  public getDataLabels(parStart: number = 0, parEnd: number = 9999): string[] {
    return [];
  }


  getDateOfBirth() {
    return (this.DATA as any).dateOfBirth;
  }


  /**
   * Insert the data into the chart structure
   * @param parRow 
   * @param parTarget 
   * @param parLabels 
   */
  insertData(parRow, parTarget, parLabels: string[]) {

    var dataIdx = this.dateToLabelIndex(parRow, parLabels);

    var dataTime = this.buildLabel(parLabels[dataIdx]);

    for (var a in this.dataRange) {
      var dIdx = this.dataRange[a];
      parTarget[a].data[dataIdx] = {
        x: dataTime,
        y: parRow[dIdx]
      }
    }

  }

}