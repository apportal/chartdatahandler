import Def from '../utilities';
import DataHandler from '../DataHandler';
///import { DataLoader, UrlTypes} from '../../DataLoader';
import { ChartDataChannel, DataSets } from './../datatypes'
import utilities from '../utilities';
import { ParseOptions } from '../datatypes/ParseOptions';


export class MYMOP_Handler extends DataHandler {

  private dataRange: number[];
  public SUB_RAW_DATA;

  constructor(sOption) {
    super(sOption);
    this.dataRange = Object.keys( this.surveyOptions.data_mapping ).map(key=>Number(key))
    //console.log(this.dataRange)
  }

  public parse(parRawData, parOptions: ParseOptions, parCallback?: Function) {
    var $this = this;
    this.DATA = this.parseLimesurveyRawData(parRawData);
    for (var a = 0; a < 4; a++) {
      this.applyProp(this.DATA.dataItems[a], {
        id: a,
        pointRadius: 4,
        pointHoverRadius: 4,
        fill: false
      });
      this.applyColor(this.DATA.dataItems[a], ['#cb4b4b', '#edc240', '#afd8f8', '#3d853d'][a], 100, 100);
    }

    if (this.surveyOptions.followupData != undefined) {
      if (!utilities.test(parOptions.followupData, 'Follow Data is needed but [followupData] was not provided')) {
        parCallback(this.DATA)
        return
      }

      var followData = this.parseLimesurveyRawData(parOptions.followupData)
      var dLabels = followData.labels;
      var dSets = followData.dataItems;

      for (var a = 0; a < dSets.length; a++) {
        this.DATA.dataItems[a].data = this.DATA.dataItems[a].data.concat(dSets[a].data);
      }

      this.DATA.labels = this.DATA.labels.concat(dLabels);
      this.SUB_RAW_DATA = parOptions.followupData
    }

    this.updateInterface();
    this.DATA.surveyOptions = this.surveyOptions;
    parCallback(this.DATA);
  }



  /**
   *
   *
   * @memberof TsqmHandler
   */
  public updateInterface() {
    var lablesList: string[] = Object.keys( this.surveyOptions.data_mapping ).map( key=>this.surveyOptions.data_mapping[key] );

    for (var a = 0; a < this.surveyOptions.datarange_indexs.length; a++) {

      var idx: number = this.surveyOptions.datarange_indexs[a]; // get index on data list
      var label: string = lablesList[a];

      if (label.indexOf('[') == 0) {
        var opt = label.replace(/\[|\]/gm, '').split(':');
        switch (opt[0].toLowerCase()) {
          case 'fromcol':
            idx = Number(opt[1]);
            label = undefined;
            break;
          default:
        }
      }

      if (typeof label == 'undefined' || label == '') {
        label = this.RAW_DATA[idx];
      }

      this.DATA.dataItems[a].label = label;

      //$('.chart_wrapper').append($('<div id="cLegend_' + a + '" class="legend col_' + (a + 1) + '">' + this.DATA.dataItems[a].label + '</div>'));
    }
    /*
    this.surveyOptions.

    this.DATA.dataItems[0].label = this.RAW_DATA[8];
    this.DATA.dataItems[1].label = this.RAW_DATA[9];
    this.DATA.dataItems[2].label = this.RAW_DATA[12];

     $('.chart_wrapper').append( $('<div id="cLegend_0" class="legend col_1">'+ this.DATA.dataItems[0].label +'</div>')  );
     $('.chart_wrapper').append( $('<div id="cLegend_1" class="legend col_2">'+ this.DATA.dataItems[1].label +'</div>')  );
     $('.chart_wrapper').append( $('<div id="cLegend_2" class="legend col_3">'+ this.DATA.dataItems[2].label +'</div>')  );
     $('.chart_wrapper').append( $('<div id="cLegend_3" class="legend col_4">'+ this.DATA.dataItems[3].label +'</div>')  );
     */
  }



  /**
   * Parse and prepare the chart datastructure
   * @param parRawData 
   */
  public parseLimesurveyRawData(parRawData: any): DataSets {

    var ls_dataObj = this.dataToObject(parRawData.responses);

    this.RAW_DATA = ls_dataObj[0];

    //console.log('ls_dataO',parRawData);

    // build the horizontal labels list
    var dataLables: string[] = this.getHorizontalLabels(ls_dataObj);

    // build the inital Datastructure
    var dataItems: ChartDataChannel[] = this.buildDataStructure(dataLables.length);

    console.log(dataLables);
    //process each row and insert data into chart data object
    for (var a = 0; a < ls_dataObj.length; a++) {
      this.insertData(ls_dataObj[a], dataItems, dataLables);
    }

    return ({
      title: Def.defaultVal(this.surveyOptions.title, parRawData.title, 'surveyOptions.title'),
      description: Def.defaultVal(this.surveyOptions.subtitle, parRawData.welcomeText, 'surveyOptions.title'),
      maxValue: this.surveyOptions.data_maxvalue,
      labels: dataLables,
      recordCount: ls_dataObj.length,
      dataItems: dataItems
    } as DataSets);

  }


  public getDataLabels(parStart: number = 0, parEnd: number = 9999): string[] {
    return this.DATA.labels;
  }

  /**
   * 
   * 
   * @param {string} parDateString 
   * @returns 
   * @memberof dataLoader
   */
  buildLabel(parDateString: String) {
    var sp = String(parDateString.split(' ')[0]).split('-');
    return sp[0] + '-' + sp[1] + '-' + sp[2];
  }



  /**
   * Insert the data into the chart structure
   * @param parRow 
   * @param parTarget 
   * @param parLabels 
   */
  insertData(parRow, parTarget, parLabels: string[]) {
    var dataIdx = this.dateToLabelIndex(parRow, parLabels);
    var dataTime = this.buildLabel(parLabels[dataIdx]);

    //dataIdx = 0;
    for (var a in this.dataRange) {

      var dIdx = this.dataRange[a];

      parTarget[a].data[dataIdx] = {
        x: dataTime,
        y: parRow[dIdx]
      }

    }

  }

}