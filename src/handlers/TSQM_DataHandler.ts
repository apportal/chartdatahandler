import Def from '../utilities';
import DataHandler from '../DataHandler';
import {ChartDataChannel,DataSetLabels,DataSets} from './../datatypes'
import { ParseOptions } from '../datatypes/ParseOptions';


export class TSQM_Handler extends DataHandler {

  private dataRange: number[];

  constructor(sOption) {
    super(sOption);
    this.dataRange = this.surveyOptions.datarange_indexs.slice();
  }

  /**
   * 
   * @param parRawData 
   */
  public parse(parRawData:any, parOptions:ParseOptions , parCallback?:Function){

    this.DATA = this.parseLimesurveyRawData(parRawData);

    for (var a = 0; a < 3; a++) {
      this.applyProp(this.DATA.dataItems[a], {
        id: a,
        pointRadius: 4,
        pointHoverRadius: 4,
        fill: false
      });

      this.applyColor(this.DATA.dataItems[a], ['#cb4b4b', '#edc240', '#afd8f8'][a], 100, 100);
    }
    this.DATA.surveyOptions = this.surveyOptions;
    parCallback.call(this, this.DATA);
  }



  /**
   * Parse and prepare the chart datastructure
   * @param parRawData 
   */
  public parseLimesurveyRawData(parRawData: any): DataSets {

    var ls_dataObj = this.dataToObject(parRawData.responses);

    // build the horizontal labels list
    var dataLables: string[] = this.getHorizontalLabels(ls_dataObj);

    // build the inital Datastructure
    var dataItems: ChartDataChannel[] = this.buildDataStructure(dataLables.length);

    //process each row and insert data into chart data object
    for (var a = 0; a < ls_dataObj.length; a++) {
      this.insertData(ls_dataObj[a], dataItems, dataLables);
    }

    return ({
      title: Def.defaultVal(this.surveyOptions.title, parRawData.title, 'surveyOptions.title'),
      description: Def.defaultVal(this.surveyOptions.subtitle, parRawData.welcomeText, 'surveyOptions.title'),
      maxValue: this.surveyOptions.data_maxvalue,
      labels: dataLables,
      recordCount: ls_dataObj.length,
      dataItems: dataItems
    } as DataSets);

  }



  /**
   * Insert the data into the chart structure
   * @param parRow 
   * @param parTarget 
   * @param parLabels 
   */
  insertData(parRow, parTarget, parLabels: string[]) {
    var dataIdx = this.dateToLabelIndex(parRow, parLabels);
    var dataTime = this.buildLabel(parLabels[dataIdx]);

    for (var a in this.dataRange) {
      var dIdx = this.dataRange[a];
      parTarget[a].data[dataIdx] = {
        x: dataTime,
        y: parRow[dIdx]
      }
    }

  }

}