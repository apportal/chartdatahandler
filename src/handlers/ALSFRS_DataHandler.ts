
import Def from '../utilities';
import DataHandler from '../DataHandler';
import {ChartDataChannel, DataSetLabels, DataSets } from './../datatypes'
import { ParseOptions } from '../datatypes/ParseOptions';

export class ALSFRS_Handler extends DataHandler {

  private D_MIN: number;
  private D_MAX: number;
  private D_RANGE: number[];
  private DATE_IDX: number = 0;
  private DATA_LABELS: string[];
  private DATA_OFFSET = 7;
  private DATA_SUM: ChartDataChannel;
  private DATA_cross;

  /**
   * 
   * @param sOption 
   */
  constructor(sOption) {
    super(sOption);
    this.D_MIN = this.surveyOptions.datarange_start;
    this.D_MAX = this.surveyOptions.datarange_end;
    this.DATA_LABELS = Object.keys( this.surveyOptions.data_mapping ).map( key=>this.surveyOptions.data_mapping[key] )
    this.DATE_IDX = this.surveyOptions.data_datefieldindex;

    // if (this.surveyOptions.datarange_indexs) {
    //   this.D_RANGE = this.surveyOptions.datarange_indexs.slice();
    //   console.log('XXdrange',this.D_RANGE)
    // }

    if (this.surveyOptions.data_mapping){
      this.D_RANGE = Object.keys( this.surveyOptions.data_mapping ).map(key=>Number(key))
    }

  }

  /**
   * 
   *
   * @param {*} parRawData
   * @returns
   * @memberof alsfrsHandler
   */
  public parse(parRawData, parOptions:ParseOptions, parCallback?:Function){
    
    this.DATA = this.parseLimesurveyRawData(parRawData.responses);
    console.log('RAW',this.DATA)

    this.DATA.title = Def.defaultVal(this.surveyOptions.title, parRawData.title, 'surveyOptions.title');
    this.DATA.description = Def.defaultVal(this.surveyOptions.subtitle, parRawData.welcomeText, 'dataCfg.subtitle');
    this.DATA.maxValue = this.surveyOptions.data_maxvalue;
    this.DATA_cross = {};

    for (var a = 0; a < this.DATA.dataItems.length; a++) {
      this.DATA.dataItems[a].id = a;
      this.applyColor(this.DATA.dataItems[a], a, 50, 20);
      this.applyProp(this.DATA.dataItems[a], {
        cubicInterpolationMode: 'monotone',
        pointRadius: 8,
        pointHoverRadius: 8,
        pointHitRadius: 8
      });
      this.DATA_cross[a] = this.DATA.dataItems[a];
    }
    this.DATA.dataItems['sum'] = this.buildTotal();
    this.DATA_cross['sum'] = this.buildTotal();

    this.DATA.surveyOptions = this.surveyOptions;
    parCallback.call(this, this.DATA);
  }

  /**
   * 
   * 
   * @param {*} parResponses 
   * @returns {dataSets} 
   * @memberof dataLoader
   */
  public parseLimesurveyRawData(parResponses: any): DataSets {
    var a: number;
    var b: string;

    // convert the raw data to a object
    var ls_dataObj = this.dataToObject(parResponses);

    console.log('>>>',ls_dataObj)

    // build the list of horizontal labels
    var dataLables: String[] = this.getHorizontalLabels(ls_dataObj);

    // Build the initial - empty datastructure based on the horizonal Labels
    var dataItems = this.buildDataStructure(dataLables.length);

    for (a = 0; a < ls_dataObj.length; a++) {

      if (ls_dataObj[a][0] == 'id') {
        continue;
      }

      var dataIdx = this.dateToLabelIndex(ls_dataObj[a], dataLables);
      var dataTime = this.buildLabel(dataLables[dataIdx]);

      /*
      //var fullDateTime = ls_dataObj[a][1];
      var fullDateTime = ls_dataObj[a][ this.DATE_IDX ];
      var dataTime = this.buildLabel( fullDateTime );
      var dataIdx  = dataLables.indexOf( dataTime );
      */

      var part
      if (this.D_RANGE) {
        part = ls_dataObj[a].filter((itm, i) => {
          return this.D_RANGE.indexOf(i) != -1;
        })
      } else {
        part = ls_dataObj[a].slice(this.D_MIN, this.D_MAX);
      }

      part.forEach((el, idx) => {
        dataItems[idx].data[dataIdx] = {
          x: dataTime,
          y: el
        };
      });
    }

    return ({
      title: '',
      description: '',
      maxValue: 0,
      labels: dataLables,
      recordCount: ls_dataObj.length,
      dataItems: dataItems
    } as DataSets);

  }

  public buildLabel(parDateString: String) {
    var sp = String(parDateString.split(' ')[0]).split('-');
    return sp[0] + '-' + sp[1]+ '-' + sp[2];
  }


  /**
   * 
   * 
   * @returns 
   * @memberof dataLoader
   */
  buildDataStructure(dataArryLength) {
    var res = [];
    var a;
    for (a = 0; a < this.DATA_LABELS.length; a++) {
      res.push({
        "label": this.DATA_LABELS[a],
        "data": new Array(dataArryLength)
        //"data" : new Array(  )
      });
    }
    return res;
  }



  /**
   * 
   */
  buildTotal() {

    var res: ChartDataChannel = {...Def.defaultStyle,...{
      label: 'Gesamtwert',
      backgroundColor: Def.convertHex(Def.blue, 20),
      borderColor: Def.convertHex(Def.blue, 60),
      data: []
    }}

    var count: number = this.DATA.labels.length;

    for (var a = 0; a < count; a++) {
      var entryVal = 0;
      var entryDate = null;
      for (var q in this.DATA.dataItems) {
        var curSet = this.DATA.dataItems[q];

        entryVal += Number(curSet.data[a].y);//curSet[ a ];
        entryDate = curSet.data[a].x;
      }
      res.data.push({
        x: entryDate,
        y: entryVal
      });
    }

    return res;
  }




  /**
   * Get the horizontals labels within min und max
   * 
   * @param {number} [parStart=0] 
   * @param {number} [parEnd=9999] 
   * @returns 
   * 
   * @memberOf dataLoader
   */
  getDataLabels(parStart: number = 0, parEnd: number = 9999): string[] {
    var res: string[] = [];

    for (var a = 0; a < this.DATA.labels.length; a++) {
      if (a >= parStart && a <= parEnd) {
        res.push(this.DATA.labels[a]);
      }

    }
    return res;
  }



  /**
   * Build a Array with id and title for the filters   - this is used fro the options
   * 
   * @returns {dataSetLabels[]} 
   * 
   * @memberOf dataLoader
   */
  getDataTitles(): DataSetLabels[] {
    var res: DataSetLabels[] = [];
    for (var a = 0; a < this.DATA.dataItems.length; a++) {
      res.push({
        id: this.DATA.dataItems[a].id,
        selected: false,
        color: Def.chartColorList[a],
        title: this.DATA.dataItems[a].label,
      });
    }
    return res;
  }



  /**
   * 
   * 
   * @param {string[]} parIds 
   * @param {number} [parStart=0] 
   * @param {number} [parEnd=9999] 
   * 
   * @memberOf dataLoader
   */
  getDataSets(parIds?: string[], parStart: number = 0, parEnd: number = 9999) {

    var res = [];

    for (var a in parIds) {
      var curId = parIds[a];
      var curSet: ChartDataChannel = this.DATA_cross[curId];

      if (typeof curSet._data == 'undefined') {
        curSet._data = curSet.data.slice(0);
      }

      curSet.data = [];
      curSet.sid = curId;
      curSet._data.forEach(function (val, idx) {
        if (idx >= parStart && idx <= parEnd) {
          curSet.data.push(val)
        }
      });

      res.push(curSet);
    }

    return res;

  }



  public getItemCount() {
    return this.DATA.dataItems[0].data.length;
  }

}
