const { merge } = require('webpack-merge');
 const common = require('./webpack.common.js');
const path = require('path');

module.exports = merge(common,{
  mode:'production',
  entry: './src/package.ts',
  output: {
    path: path.join(__dirname, './build'),
    filename: 'ap-datahandler.package.js',
  },  
});