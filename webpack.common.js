const path = require('path');

module.exports = {
  entry: './src/index.ts',
  resolve: {
    extensions: [".ts", ".js",'.json'],
  },
  module: {
    rules: [
      {
        test: /\.ts$/i,
        use: ["ts-loader"],
      },
    ],
  }
};

