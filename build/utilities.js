"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var default_1 = /** @class */ (function () {
    function default_1() {
    }
    /**
     * Test a value and return a default if value is not set
     *
     * @param {any} parVal
     * @param {any} parDefault
     * @returns
     * @memberof dataLoader
     */
    default_1.defaultVal = function (parVal, parDefault, parValName) {
        if (parValName === void 0) { parValName = ''; }
        if (parVal) {
            return parVal;
        }
        else {
            console.warn('(' + parValName + ') Value not set - default to:', parDefault);
            return parDefault;
        }
    };
    default_1.test = function (value, errorText) {
        if (typeof value === 'undefined') {
            console.log('ERROR: %c' + errorText, 'color: #ffcc00');
            return false;
        }
        return true;
    };
    /**
     *
     * @param hex
     * @param opacity
     * @returns
     */
    default_1.convertHex = function (hex, opacity) {
        hex = hex.replace('#', '');
        var r = parseInt(hex.substring(0, 2), 16);
        var g = parseInt(hex.substring(2, 4), 16);
        var b = parseInt(hex.substring(4, 6), 16);
        var result = 'rgba(' + r + ',' + g + ',' + b + ',' + opacity / 100 + ')';
        return result;
    };
    /**
     *
     * @param parDate
     * @returns
     */
    default_1.mkDate = function (parDate) {
        var m = ['', 'Jan', 'Feb', 'Mär', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sept', 'Okt', 'Nov', 'Dez'];
        if (parDate.indexOf('-') != -1) {
            var d = parDate.split('-');
            return m[d[0]] + " " + d[1];
        }
        return parDate;
    };
    default_1.blue = '#126398';
    default_1.chartColorList = [
        '#edc240',
        '#afd8f8',
        '#cb4b4b',
        '#4da74d',
        '#9440ed',
        '#bd9b33',
        '#8cacc6',
        '#8e2f2f',
        '#3d853d',
        '#cd4d4c',
        '#aae0ce',
        '#b15299',
        '#829350',
        '#4c6ecd',
        '#000000',
        '#000000',
        '#000000',
        '#000000',
        '#000000',
    ];
    /**
     *
     *
     * @static
     */
    default_1.defaultStyle = {
        fill: true,
        pointRadius: 3,
        pointHitRadius: 5,
    };
    // public static surveyOptions:  config.SurveyOptions;
    default_1.DEBUGMODE = false;
    return default_1;
}());
exports.default = default_1;
;
//# sourceMappingURL=utilities.js.map