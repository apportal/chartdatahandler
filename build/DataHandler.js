"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var utilities_1 = require("./utilities");
/**
 *
 *
 * @export
 * @class dataHandler
 */
var dataHandler = /** @class */ (function () {
    function dataHandler(parSOptions) {
        this.surveyOptions = parSOptions;
    }
    /**
     *
     * @param parOpt
     */
    dataHandler.prototype.getDataMeta = function (parOpt) {
        return this.DATA[parOpt];
        //this.DATA.description         
    };
    /**
     *
     * @param parRawData
     */
    dataHandler.prototype.parse = function (parRawData, parSOptions, parCallback) {
        parCallback.call(this, null);
    };
    /**
     *
     */
    dataHandler.prototype.getDataTitles = function () {
        return [];
    };
    /**
     *
     * @param parIds
     * @param parStart
     * @param parEnd
     */
    dataHandler.prototype.getDataSets = function () {
        return this.DATA.dataItems;
    };
    /**
     *
     * @param parStart
     * @param parEnd
     */
    dataHandler.prototype.getDataLabels = function (parStart, parEnd) {
        if (parStart === void 0) { parStart = 0; }
        if (parEnd === void 0) { parEnd = 9999; }
        return [];
    };
    /**
     *
     * @param parItem
     * @param parColor
     * @param parLineAlpha
     * @param parFillAlpha
     */
    dataHandler.prototype.applyColor = function (parItem, parColor, parLineAlpha, parFillAlpha) {
        var col = (typeof parColor == 'number') ? utilities_1.default.chartColorList[parColor] : parColor;
        parItem.backgroundColor = utilities_1.default.convertHex(col, parFillAlpha);
        parItem.borderColor = utilities_1.default.convertHex(col, parLineAlpha);
    };
    /**
     *
     * @param parItem
     * @param parProps
     */
    dataHandler.prototype.applyProp = function (parItem, parProps) {
        for (var a in parProps) {
            parItem[a] = parProps[a];
        }
    };
    /**
     * This takes the RAW dataresonse as string.
     * - First it decodes the string from Unicode
     * - Removes any Formulars from the legend row
     * - Replaces all linebreaks with brackes so the string can be parsed as a Json object
     * - All illigal rows (legend) are removed from dataset
     * - at the end, that data is sorted by the Date index number from the config.
     * @param parDataStr
     */
    dataHandler.prototype.dataToObject = function (parDataStr) {
        var _this = this;
        var dataDecodede = this.b64DecodeUnicode(parDataStr);
        var dataObj;
        //replace fomulars in titles
        dataDecodede = dataDecodede.replace(new RegExp(/\"\{(.|n)*?\}\"/gm), '"Formular"');
        //replace breaks with array brackets
        dataDecodede = "[[" + dataDecodede.replace(new RegExp("\n", 'g'), '],[') + "]]";
        //try to convert the data to a object with json decode
        try {
            dataObj = JSON.parse(dataDecodede);
        }
        catch (e) {
            console.error('DATA ERROR', dataDecodede);
            return [];
        }
        if (utilities_1.default.DEBUGMODE) {
            this.logColumnTitles(dataObj[0]);
        }
        //filter out bad records
        dataObj = dataObj.filter(function (a) {
            if (a.length == 0) {
                console.error('DATASET-ERROR', 'data-length is 0');
                return false;
            }
            if (a[1] == '') {
                console.error('DATASET-ERROR', 'Data has not been completed (ABGESCHLOSSEN)');
                return false;
            }
            if (a[0] == 'id') {
                console.warn('DATASET-SKIPPING', 'found legend');
                return false;
            }
            return true;
        });
        var dIndex = this.surveyOptions.data_datefieldindex;
        //sort the data by date
        dataObj = dataObj.sort(function (a, b) {
            //var aa = new Date(a[1]);
            //var bb = new Date(b[1]);
            //var aa = this.buildLabel(a[1]);
            //var bb = this.buildLabel(b[1]);
            var aa = _this.buildLabel(a[dIndex]);
            var bb = _this.buildLabel(b[dIndex]);
            return aa < bb ? -1 : aa > bb ? 1 : 0;
        });
        return dataObj;
    };
    /**
     *
     * @param parColumns
     */
    dataHandler.prototype.logColumnTitles = function (parColumns) {
        console.log(parColumns);
    };
    /**
     *
     *
     * @returns
     * @memberof dataLoader
     */
    dataHandler.prototype.buildDataStructure = function (dataArryLength) {
        // get copy of labels from config. 
        // Labels = The types of data - NOT the labels of X or Y
        var parLabels = this.surveyOptions.datalabels.slice();
        var res = [];
        var a;
        for (a = 0; a < parLabels.length; a++) {
            res.push({
                "label": parLabels[a],
                "data": new Array(dataArryLength),
                "backgroundColor": "#000"
            });
        }
        return res;
    };
    /**
     * Itterate over data to identify the horizontal Labels
     * @param ls_dataObj
     */
    dataHandler.prototype.getHorizontalLabels = function (ls_dataObj) {
        //build labels
        var dataLabelsObj = {};
        var dataLables = [];
        var dIndex = this.surveyOptions.data_datefieldindex;
        // we first build a object with labels and count each item
        for (var a = 0; a < ls_dataObj.length; a++) {
            if (ls_dataObj[a][0] == 'id') {
                continue;
            }
            //var labelId = this.buildLabel( ls_dataObj[a][1] );
            var labelId = this.buildLabel(ls_dataObj[a][dIndex]);
            dataLabelsObj[labelId] = dataLabelsObj[labelId] ? dataLabelsObj[labelId] + 1 : 1;
        }
        for (var b in dataLabelsObj) {
            dataLables.push(b);
        }
        return dataLables;
    };
    dataHandler.prototype.dateToLabelIndex = function (parRow, parLabels) {
        var dIndex = this.surveyOptions.data_datefieldindex;
        var fullDateTime = parRow[dIndex];
        var dataTime = this.buildLabel(fullDateTime);
        var dataIdx = parLabels.indexOf(dataTime);
        return dataIdx;
    };
    /**
     *
     *
     * @param {string} parDateString
     * @returns
     * @memberof dataLoader
     */
    dataHandler.prototype.buildLabel = function (parDateString) {
        var sp = String(parDateString.split(' ')[0]).split('-');
        return sp[0] + '-' + sp[1];
    };
    /**
     *
     *
     * @param {any} str
     * @returns
     * @memberof dataLoader
     */
    dataHandler.prototype.b64DecodeUnicode = function (str) {
        // Going backwards: from bytestream, to percent-encoding, to original string.
        return decodeURIComponent(atob(str).split('').map(function (c) {
            return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
        }).join(''));
    };
    /**
     *
     * @param start Generate random date
     * @param end
     */
    dataHandler.prototype.randomDate = function (start, end) {
        var dd = new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
        var f = function (v) {
            return (v < 10 ? '0' + v : v);
        };
        return dd.getFullYear() + '-' + f(dd.getMonth() + 1) + '-' + f(dd.getDate()) + ' ' + dd.getHours() + ':' + dd.getMinutes() + ':00';
    };
    return dataHandler;
}());
exports.default = dataHandler;
//# sourceMappingURL=DataHandler.js.map