"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.NFL_Handler = void 0;
var utilities_1 = require("../utilities");
var DataHandler_1 = require("../DataHandler");
var dayjs_1 = require("dayjs");
var NFL_Handler = /** @class */ (function (_super) {
    __extends(NFL_Handler, _super);
    function NFL_Handler(sOption) {
        return _super.call(this, sOption) || this;
    }
    /**
     *
     * @param parRawData
     */
    NFL_Handler.prototype.parse = function (parRawData, parOptions, parCallback) {
        this.DATA = this.parseLimesurveyRawData(parRawData);
        this.DATA.surveyOptions = this.surveyOptions;
        parCallback.call(this, this.DATA);
    };
    /**
     * Parse and prepare the chart datastructure
     * @param parRawData
     */
    NFL_Handler.prototype.parseLimesurveyRawData = function (parRawData) {
        var ls_dataObj = this.dataToObject(parRawData.responses);
        // build the horizontal labels list
        var dataLables = [];
        for (var a = 0; a < ls_dataObj.length; a++) {
            dataLables.push(this.buildLabel(ls_dataObj[a][10]));
        }
        // build the inital Datastructure
        var dataItems = []; //this.buildDataStructure( ['dataLables'] );
        var dateOfBirth = '';
        dataItems.push({
            label: 'Data',
            data: ls_dataObj.map(function (obj) {
                //{x:0,y:10},
                dateOfBirth = String(obj[13]);
                return {
                    x: (0, dayjs_1.default)(String(obj[10])).format('YYYY-MM'),
                    y: obj[11],
                    fulldate: (0, dayjs_1.default)(String(obj[10])),
                    label: obj[12],
                };
            })
        });
        // NOTE - we dont need this - only one recorde set 
        // //process each row and insert data into chart data object
        // for( var a=0; a < ls_dataObj.length; a++){
        //     this.insertData(ls_dataObj[a], dataItems, dataLables);
        // }
        return {
            title: utilities_1.default.defaultVal(this.surveyOptions.title, parRawData.title, 'surveyOptions.title'),
            description: utilities_1.default.defaultVal(this.surveyOptions.subtitle, parRawData.welcomeText, 'surveyOptions.title'),
            dateOfBirth: dateOfBirth,
            maxValue: this.surveyOptions.data_maxvalue,
            labels: dataLables,
            recordCount: ls_dataObj.length,
            dataItems: dataItems
        };
    };
    /**
     *
     *
     * @param {string} parDateString
     * @returns
     * @memberof dataLoader
     */
    NFL_Handler.prototype.buildLabel = function (parDateString) {
        var sp = String(parDateString.split(' ')[0]).split('-');
        return sp[0] + '-' + sp[1] + '-' + sp[2];
    };
    NFL_Handler.prototype.getDataLabels = function (parStart, parEnd) {
        if (parStart === void 0) { parStart = 0; }
        if (parEnd === void 0) { parEnd = 9999; }
        return [];
    };
    NFL_Handler.prototype.getDateOfBirth = function () {
        return this.DATA.dateOfBirth;
    };
    /**
     * Insert the data into the chart structure
     * @param parRow
     * @param parTarget
     * @param parLabels
     */
    NFL_Handler.prototype.insertData = function (parRow, parTarget, parLabels) {
        var dataIdx = this.dateToLabelIndex(parRow, parLabels);
        var dataTime = this.buildLabel(parLabels[dataIdx]);
        for (var a in this.dataRange) {
            var dIdx = this.dataRange[a];
            parTarget[a].data[dataIdx] = {
                x: dataTime,
                y: parRow[dIdx]
            };
        }
    };
    return NFL_Handler;
}(DataHandler_1.default));
exports.NFL_Handler = NFL_Handler;
//# sourceMappingURL=NFL_DataHandler.js.map