"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.TSQM_Handler = void 0;
var utilities_1 = require("../utilities");
var DataHandler_1 = require("../DataHandler");
var TSQM_Handler = /** @class */ (function (_super) {
    __extends(TSQM_Handler, _super);
    function TSQM_Handler(sOption) {
        var _this = _super.call(this, sOption) || this;
        _this.dataRange = _this.surveyOptions.datarange_indexs.slice();
        return _this;
    }
    /**
     *
     * @param parRawData
     */
    TSQM_Handler.prototype.parse = function (parRawData, parOptions, parCallback) {
        this.DATA = this.parseLimesurveyRawData(parRawData);
        for (var a = 0; a < 3; a++) {
            this.applyProp(this.DATA.dataItems[a], {
                id: a,
                pointRadius: 4,
                pointHoverRadius: 4,
                fill: false
            });
            this.applyColor(this.DATA.dataItems[a], ['#cb4b4b', '#edc240', '#afd8f8'][a], 100, 100);
        }
        this.DATA.surveyOptions = this.surveyOptions;
        parCallback.call(this, this.DATA);
    };
    /**
     * Parse and prepare the chart datastructure
     * @param parRawData
     */
    TSQM_Handler.prototype.parseLimesurveyRawData = function (parRawData) {
        var ls_dataObj = this.dataToObject(parRawData.responses);
        // build the horizontal labels list
        var dataLables = this.getHorizontalLabels(ls_dataObj);
        // build the inital Datastructure
        var dataItems = this.buildDataStructure(dataLables.length);
        //process each row and insert data into chart data object
        for (var a = 0; a < ls_dataObj.length; a++) {
            this.insertData(ls_dataObj[a], dataItems, dataLables);
        }
        return {
            title: utilities_1.default.defaultVal(this.surveyOptions.title, parRawData.title, 'surveyOptions.title'),
            description: utilities_1.default.defaultVal(this.surveyOptions.subtitle, parRawData.welcomeText, 'surveyOptions.title'),
            maxValue: this.surveyOptions.data_maxvalue,
            labels: dataLables,
            recordCount: ls_dataObj.length,
            dataItems: dataItems
        };
    };
    /**
     * Insert the data into the chart structure
     * @param parRow
     * @param parTarget
     * @param parLabels
     */
    TSQM_Handler.prototype.insertData = function (parRow, parTarget, parLabels) {
        var dataIdx = this.dateToLabelIndex(parRow, parLabels);
        var dataTime = this.buildLabel(parLabels[dataIdx]);
        for (var a in this.dataRange) {
            var dIdx = this.dataRange[a];
            parTarget[a].data[dataIdx] = {
                x: dataTime,
                y: parRow[dIdx]
            };
        }
    };
    return TSQM_Handler;
}(DataHandler_1.default));
exports.TSQM_Handler = TSQM_Handler;
//# sourceMappingURL=TSQM_DataHandler.js.map