"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ALSFRS_Handler = void 0;
var utilities_1 = require("../utilities");
var DataHandler_1 = require("../DataHandler");
var ALSFRS_Handler = /** @class */ (function (_super) {
    __extends(ALSFRS_Handler, _super);
    /**
     *
     * @param sOption
     */
    function ALSFRS_Handler(sOption) {
        var _this = _super.call(this, sOption) || this;
        _this.DATE_IDX = 0;
        _this.DATA_OFFSET = 7;
        _this.D_MIN = _this.surveyOptions.datarange_start;
        _this.D_MAX = _this.surveyOptions.datarange_end;
        _this.DATA_LABELS = _this.surveyOptions.datalabels.slice();
        _this.DATE_IDX = _this.surveyOptions.data_datefieldindex;
        if (_this.surveyOptions.datarange_indexs) {
            _this.D_RANGE = _this.surveyOptions.datarange_indexs.slice();
        }
        return _this;
    }
    /**
     *
     *
     * @param {*} parRawData
     * @returns
     * @memberof alsfrsHandler
     */
    ALSFRS_Handler.prototype.parse = function (parRawData, parOptions, parCallback) {
        this.DATA = this.parseLimesurveyRawData(parRawData.responses);
        this.DATA.title = utilities_1.default.defaultVal(this.surveyOptions.title, parRawData.title, 'surveyOptions.title');
        this.DATA.description = utilities_1.default.defaultVal(this.surveyOptions.subtitle, parRawData.welcomeText, 'dataCfg.subtitle');
        this.DATA.maxValue = this.surveyOptions.data_maxvalue;
        this.DATA_cross = {};
        for (var a = 0; a < this.DATA.dataItems.length; a++) {
            this.DATA.dataItems[a].id = a;
            this.applyColor(this.DATA.dataItems[a], a, 50, 20);
            this.applyProp(this.DATA.dataItems[a], {
                cubicInterpolationMode: 'monotone',
                pointRadius: 8,
                pointHoverRadius: 8,
                pointHitRadius: 8
            });
            this.DATA_cross[a] = this.DATA.dataItems[a];
        }
        this.DATA.dataItems['sum'] = this.buildTotal();
        this.DATA_cross['sum'] = this.buildTotal();
        this.DATA.surveyOptions = this.surveyOptions;
        parCallback.call(this, this.DATA);
    };
    /**
     *
     *
     * @param {*} parResponses
     * @returns {dataSets}
     * @memberof dataLoader
     */
    ALSFRS_Handler.prototype.parseLimesurveyRawData = function (parResponses) {
        var _this = this;
        var a;
        var b;
        // convert the raw data to a object
        var ls_dataObj = this.dataToObject(parResponses);
        // build the list of horizontal labels
        var dataLables = this.getHorizontalLabels(ls_dataObj);
        // Build the initial - empty datastructure based on the horizonal Labels
        var dataItems = this.buildDataStructure(dataLables.length);
        for (a = 0; a < ls_dataObj.length; a++) {
            if (ls_dataObj[a][0] == 'id') {
                continue;
            }
            var dataIdx = this.dateToLabelIndex(ls_dataObj[a], dataLables);
            var dataTime = this.buildLabel(dataLables[dataIdx]);
            /*
            //var fullDateTime = ls_dataObj[a][1];
            var fullDateTime = ls_dataObj[a][ this.DATE_IDX ];
            var dataTime = this.buildLabel( fullDateTime );
            var dataIdx  = dataLables.indexOf( dataTime );
            */
            var part;
            if (this.D_RANGE) {
                part = ls_dataObj[a].filter(function (itm, i) {
                    return _this.D_RANGE.indexOf(i) != -1;
                });
            }
            else {
                part = ls_dataObj[a].slice(this.D_MIN, this.D_MAX);
            }
            part.forEach(function (el, idx) {
                dataItems[idx].data[dataIdx] = {
                    x: dataTime,
                    y: el
                };
            });
        }
        return {
            title: '',
            description: '',
            maxValue: 0,
            labels: dataLables,
            recordCount: ls_dataObj.length,
            dataItems: dataItems
        };
    };
    /**
     *
     *
     * @returns
     * @memberof dataLoader
     */
    ALSFRS_Handler.prototype.buildDataStructure = function (dataArryLength) {
        var res = [];
        var a;
        for (a = 0; a < this.DATA_LABELS.length; a++) {
            res.push({
                "label": this.DATA_LABELS[a],
                "data": new Array(dataArryLength)
                //"data" : new Array(  )
            });
        }
        return res;
    };
    /**
     *
     */
    ALSFRS_Handler.prototype.buildTotal = function () {
        var res = __assign(__assign({}, utilities_1.default.defaultStyle), {
            label: 'Gesamtwert',
            backgroundColor: utilities_1.default.convertHex(utilities_1.default.blue, 20),
            borderColor: utilities_1.default.convertHex(utilities_1.default.blue, 60),
            data: []
        });
        var count = this.DATA.labels.length;
        for (var a = 0; a < count; a++) {
            var entryVal = 0;
            var entryDate = null;
            for (var q in this.DATA.dataItems) {
                var curSet = this.DATA.dataItems[q];
                entryVal += Number(curSet.data[a].y); //curSet[ a ];
                entryDate = curSet.data[a].x;
            }
            res.data.push({
                x: entryDate,
                y: entryVal
            });
        }
        return res;
    };
    /**
     * Get the horizontals labels within min und max
     *
     * @param {number} [parStart=0]
     * @param {number} [parEnd=9999]
     * @returns
     *
     * @memberOf dataLoader
     */
    ALSFRS_Handler.prototype.getDataLabels = function (parStart, parEnd) {
        if (parStart === void 0) { parStart = 0; }
        if (parEnd === void 0) { parEnd = 9999; }
        var res = [];
        for (var a = 0; a < this.DATA.labels.length; a++) {
            if (a >= parStart && a <= parEnd) {
                res.push(this.DATA.labels[a]);
            }
        }
        return res;
    };
    /**
     * Build a Array with id and title for the filters   - this is used fro the options
     *
     * @returns {dataSetLabels[]}
     *
     * @memberOf dataLoader
     */
    ALSFRS_Handler.prototype.getDataTitles = function () {
        var res = [];
        for (var a = 0; a < this.DATA.dataItems.length; a++) {
            res.push({
                id: this.DATA.dataItems[a].id,
                selected: false,
                color: utilities_1.default.chartColorList[a],
                title: this.DATA.dataItems[a].label,
            });
        }
        return res;
    };
    /**
     *
     *
     * @param {string[]} parIds
     * @param {number} [parStart=0]
     * @param {number} [parEnd=9999]
     *
     * @memberOf dataLoader
     */
    ALSFRS_Handler.prototype.getDataSets = function (parIds, parStart, parEnd) {
        if (parStart === void 0) { parStart = 0; }
        if (parEnd === void 0) { parEnd = 9999; }
        var res = [];
        for (var a in parIds) {
            var curId = parIds[a];
            var curSet = this.DATA_cross[curId];
            if (typeof curSet._data == 'undefined') {
                curSet._data = curSet.data.slice(0);
            }
            curSet.data = [];
            curSet.sid = curId;
            curSet._data.forEach(function (val, idx) {
                if (idx >= parStart && idx <= parEnd) {
                    curSet.data.push(val);
                }
            });
            res.push(curSet);
        }
        return res;
    };
    ALSFRS_Handler.prototype.getItemCount = function () {
        return this.DATA.dataItems[0].data.length;
    };
    return ALSFRS_Handler;
}(DataHandler_1.default));
exports.ALSFRS_Handler = ALSFRS_Handler;
//# sourceMappingURL=ALSFRS_DataHandler.js.map