"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.MYMOP_Handler = void 0;
var utilities_1 = require("../utilities");
var DataHandler_1 = require("../DataHandler");
var utilities_2 = require("../utilities");
var MYMOP_Handler = /** @class */ (function (_super) {
    __extends(MYMOP_Handler, _super);
    function MYMOP_Handler(sOption) {
        var _this = _super.call(this, sOption) || this;
        _this.dataRange = _this.surveyOptions.datarange_indexs.slice();
        return _this;
    }
    MYMOP_Handler.prototype.parse = function (parRawData, parOptions, parCallback) {
        var $this = this;
        this.DATA = this.parseLimesurveyRawData(parRawData);
        for (var a = 0; a < 4; a++) {
            this.applyProp(this.DATA.dataItems[a], {
                id: a,
                pointRadius: 4,
                pointHoverRadius: 4,
                fill: false
            });
            this.applyColor(this.DATA.dataItems[a], ['#cb4b4b', '#edc240', '#afd8f8', '#3d853d'][a], 100, 100);
        }
        if (this.surveyOptions.followupData != undefined) {
            if (!utilities_2.default.test(parOptions.followupData, 'Follow Data is needed but [followupData] was not provided')) {
                parCallback(this.DATA);
                return;
            }
            var followData = this.parseLimesurveyRawData(parOptions.followupData);
            var dLabels = followData.labels;
            var dSets = followData.dataItems;
            for (var a = 0; a < dSets.length; a++) {
                this.DATA.dataItems[a].data = this.DATA.dataItems[a].data.concat(dSets[a].data);
            }
            this.DATA.labels = this.DATA.labels.concat(dLabels);
            this.SUB_RAW_DATA = parOptions.followupData;
        }
        this.updateInterface();
        this.DATA.surveyOptions = this.surveyOptions;
        parCallback(this.DATA);
    };
    /**
     *
     *
     * @memberof TsqmHandler
     */
    MYMOP_Handler.prototype.updateInterface = function () {
        for (var a = 0; a < this.surveyOptions.datarange_indexs.length; a++) {
            var idx = this.surveyOptions.datarange_indexs[a]; // get index on data list
            var label = this.surveyOptions.datalabels[a];
            if (label.indexOf('[') == 0) {
                var opt = label.replace(/\[|\]/gm, '').split(':');
                switch (opt[0].toLowerCase()) {
                    case 'fromcol':
                        idx = Number(opt[1]);
                        label = undefined;
                        break;
                    default:
                }
            }
            if (typeof label == 'undefined' || label == '') {
                label = this.RAW_DATA[idx];
            }
            this.DATA.dataItems[a].label = label;
            //$('.chart_wrapper').append($('<div id="cLegend_' + a + '" class="legend col_' + (a + 1) + '">' + this.DATA.dataItems[a].label + '</div>'));
        }
        /*
        this.surveyOptions.
    
        this.DATA.dataItems[0].label = this.RAW_DATA[8];
        this.DATA.dataItems[1].label = this.RAW_DATA[9];
        this.DATA.dataItems[2].label = this.RAW_DATA[12];
    
         $('.chart_wrapper').append( $('<div id="cLegend_0" class="legend col_1">'+ this.DATA.dataItems[0].label +'</div>')  );
         $('.chart_wrapper').append( $('<div id="cLegend_1" class="legend col_2">'+ this.DATA.dataItems[1].label +'</div>')  );
         $('.chart_wrapper').append( $('<div id="cLegend_2" class="legend col_3">'+ this.DATA.dataItems[2].label +'</div>')  );
         $('.chart_wrapper').append( $('<div id="cLegend_3" class="legend col_4">'+ this.DATA.dataItems[3].label +'</div>')  );
         */
    };
    /**
     * Parse and prepare the chart datastructure
     * @param parRawData
     */
    MYMOP_Handler.prototype.parseLimesurveyRawData = function (parRawData) {
        var ls_dataObj = this.dataToObject(parRawData.responses);
        this.RAW_DATA = ls_dataObj[0];
        //console.log('ls_dataO',parRawData);
        // build the horizontal labels list
        var dataLables = this.getHorizontalLabels(ls_dataObj);
        // build the inital Datastructure
        var dataItems = this.buildDataStructure(dataLables.length);
        console.log(dataLables);
        //process each row and insert data into chart data object
        for (var a = 0; a < ls_dataObj.length; a++) {
            this.insertData(ls_dataObj[a], dataItems, dataLables);
        }
        return {
            title: utilities_1.default.defaultVal(this.surveyOptions.title, parRawData.title, 'surveyOptions.title'),
            description: utilities_1.default.defaultVal(this.surveyOptions.subtitle, parRawData.welcomeText, 'surveyOptions.title'),
            maxValue: this.surveyOptions.data_maxvalue,
            labels: dataLables,
            recordCount: ls_dataObj.length,
            dataItems: dataItems
        };
    };
    MYMOP_Handler.prototype.getDataLabels = function (parStart, parEnd) {
        if (parStart === void 0) { parStart = 0; }
        if (parEnd === void 0) { parEnd = 9999; }
        return this.DATA.labels;
    };
    /**
     *
     *
     * @param {string} parDateString
     * @returns
     * @memberof dataLoader
     */
    MYMOP_Handler.prototype.buildLabel = function (parDateString) {
        var sp = String(parDateString.split(' ')[0]).split('-');
        return sp[0] + '-' + sp[1] + '-' + sp[2];
    };
    /**
     * Insert the data into the chart structure
     * @param parRow
     * @param parTarget
     * @param parLabels
     */
    MYMOP_Handler.prototype.insertData = function (parRow, parTarget, parLabels) {
        var dataIdx = this.dateToLabelIndex(parRow, parLabels);
        var dataTime = this.buildLabel(parLabels[dataIdx]);
        //dataIdx = 0;
        for (var a in this.dataRange) {
            var dIdx = this.dataRange[a];
            parTarget[a].data[dataIdx] = {
                x: dataTime,
                y: parRow[dIdx]
            };
        }
    };
    return MYMOP_Handler;
}(DataHandler_1.default));
exports.MYMOP_Handler = MYMOP_Handler;
//# sourceMappingURL=MYMOP_DataHandler.js.map