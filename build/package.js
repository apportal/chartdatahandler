"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __exportStar = (this && this.__exportStar) || function(m, exports) {
    for (var p in m) if (p !== "default" && !Object.prototype.hasOwnProperty.call(exports, p)) __createBinding(exports, m, p);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DataHandler = void 0;
var defaultConfig = require("./config.json");
var ALSFRS_DataHandler_1 = require("./handlers/ALSFRS_DataHandler");
var MYMOP_DataHandler_1 = require("./handlers/MYMOP_DataHandler");
var NFL_DataHandler_1 = require("./handlers/NFL_DataHandler");
var TSQM_DataHandler_1 = require("./handlers/TSQM_DataHandler");
var utilities_1 = require("./utilities");
__exportStar(require("./datatypes"), exports);
var parseConfig = function (data) {
    if (!data.copy)
        return data;
    Object.keys(data.copy).forEach(function (key) {
        var entry = data[key];
        data.copy[key].forEach(function (altkey) {
            data[altkey] = __assign({}, entry);
        });
    });
    return __assign({}, data);
};
exports.DataHandler = {
    config: parseConfig(defaultConfig),
    setConfig: function (newConfig) {
        exports.DataHandler.config = __assign(__assign({}, exports.DataHandler.config), newConfig);
    },
    getConfig: function () {
        return exports.DataHandler.config;
    },
    getConfigSurveyIds: function () {
        return Object.keys(exports.DataHandler.config).join(',');
    },
    parse: function (data, cb) {
        if (!utilities_1.default.test(data, 'Data Missing, at least [surveyid] must be given'))
            return;
        if (!utilities_1.default.test(data.surveyid, "surveyid missing\nPossible Values: " + exports.DataHandler.getConfigSurveyIds()))
            return;
        if (!utilities_1.default.test(exports.DataHandler.config[data.surveyid], "surveyid [" + data.surveyid + "] not found\npossible values: " + exports.DataHandler.getConfigSurveyIds() + "."))
            return;
        var curConfig = exports.DataHandler.config[data.surveyid];
        var parser;
        switch (curConfig.type.toUpperCase()) {
            case 'ALSFRS':
                parser = new ALSFRS_DataHandler_1.ALSFRS_Handler(exports.DataHandler.config[data.surveyid]);
                break;
            case 'MYMOP':
                parser = new MYMOP_DataHandler_1.MYMOP_Handler(exports.DataHandler.config[data.surveyid]);
                break;
            case 'NFL':
                parser = new NFL_DataHandler_1.NFL_Handler(exports.DataHandler.config[data.surveyid]);
                break;
            case 'TSQM':
                parser = new TSQM_DataHandler_1.TSQM_Handler(exports.DataHandler.config[data.surveyid]);
                break;
            default:
                console.warn('Type not defined :', curConfig.type);
        }
        if (cb) {
            parser.parse(data.raw, data, function (result) {
                cb(result);
            });
        }
        else {
            return new Promise(function (resolve, reject) {
                parser.parse(data.raw, data, function (result) {
                    resolve(result);
                });
            });
        }
    }
};
//# sourceMappingURL=package.js.map